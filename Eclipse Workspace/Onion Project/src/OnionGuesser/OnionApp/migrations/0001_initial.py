# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-17 17:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article_text', models.TextField()),
                ('artilce_url', models.URLField()),
                ('thumbnail_url', models.URLField(blank=True, default='', null=True)),
                ('timestamp', models.DateTimeField()),
            ],
        ),
    ]
