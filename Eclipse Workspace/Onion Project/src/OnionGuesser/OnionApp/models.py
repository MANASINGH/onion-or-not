from django.db import models
from datetime import datetime

# Create your models here.
class Article(models.Model):
    article_text = models.TextField()
    artilce_url = models.URLField(max_length=200)
    thumbnail_url = models.URLField(max_length=200, default='', blank=True, null=True)
    timestamp = models.DateTimeField() 
    
    is_onion = models.BooleanField(default=True)
    onion_votes = models.IntegerField(default=0)
    nto_votes = models.IntegerField(default=0)
    unanswered = models.BooleanField(default=True)
    
    def __str__(self):
        return self.article_text