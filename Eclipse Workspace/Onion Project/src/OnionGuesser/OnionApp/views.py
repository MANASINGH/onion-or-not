from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponse, JsonResponse,\
    HttpResponseRedirect
from django.views.generic.list import ListView
from .models import Article
from django.views.generic.detail import DetailView
import json
from django.core import serializers
from django.core.urlresolvers import reverse

# Create your views here.
class ArticleList(ListView):
    model = Article
    context_object_name = "articles"
    
class ArticleDetail(DetailView):
    model = Article
    context_object_name = "theArticle"
    
    def get(self, request, pkey):
        #article = [Article.objects.get(pk=pkey)]
        #data = serializers.serialize("json", article )
        #print(data)
        #return HttpResponse(data, content_type='application/json')
        dict_ver = self.get_dict_ver(pkey)
        return JsonResponse(dict_ver)
    
    def get_dict_ver(self, pkey):
        dict_ver = {}
        article = Article.objects.get(pk=pkey)
        dict_ver['name'] = article.article_text
        dict_ver['url'] = article.artilce_url
        dict_ver['thumbnail_url'] = article.thumbnail_url
        dict_ver['timestamp'] = article.timestamp
        print(dict_ver)
        return dict_ver
     
    def post(self, request, pkey):
        article = get_object_or_404(Article, pk=pkey)
        print(request.POST)
        if(request.POST['onion_select'] == 'Onion'):
            article.onion_votes = article.onion_votes + 1
        elif(request.POST['onion_select'] == 'Not the Onion'):
            article.nto_votes = article.nto_votes + 1
        
        article.unanswered = False;
        article.save()
        
        return HttpResponseRedirect(reverse('OnionApp:home'))

def vote(request):
    return HttpResponse("Congrats, you've got it!")
