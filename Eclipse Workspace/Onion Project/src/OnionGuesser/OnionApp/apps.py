from django.apps import AppConfig


class OnionappConfig(AppConfig):
    name = 'OnionApp'
