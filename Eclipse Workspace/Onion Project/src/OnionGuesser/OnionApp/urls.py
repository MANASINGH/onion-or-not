# URLs for the OnionApp application
from django.conf.urls import url
from . import views
from .views import ArticleList, ArticleDetail
from django.views.generic.base import TemplateView

app_name = "OnionApp"
urlpatterns = [
    #Ex: ...OnionApp/
    url(r'^$', TemplateView.as_view(template_name="OnionApp/index.html"), name='home'),
    #Ex: ..OnionApp/1/
    url(r'^(?P<pkey>[0-9]+)/$', ArticleDetail.as_view(template_name="OnionApp/index.html"), name='detail'),
    url(r'^vote/(?P<pkey>[0-9]+)/$', views.vote, name='vote')
]